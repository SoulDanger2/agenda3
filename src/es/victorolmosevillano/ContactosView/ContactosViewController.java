/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.ContactosView;

import es.victorolmosevillano.agendacontactos.Persona;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class ContactosViewController implements Initializable {

    private EntityManager entityManager;
    private Persona personaSeleccionada;
    @FXML
    private TableView<Persona> tableViewContactos;
    @FXML
    private TableColumn<Persona, String> columnNombre;
    @FXML
    private TableColumn<Persona, String> columnApellidos;
    @FXML
    private TableColumn<Persona, String> columnEmail;
    @FXML
    private TableColumn<Persona, String> columnProvincia;
    @FXML
    private TextField textFieldNombre;

    @FXML
    private Button buttonNuevo;
    @FXML
    private Button buttonEditar;
    @FXML
    private Button buttonBorrar;
    @FXML
    private Button buttonActualizar;
    @FXML
    private AnchorPane rootContactosView;
    @FXML
    private TextField textFieldApellidos;
    @FXML
    private TextField textFieldEmail;
    @FXML
    private TextField textFieldTelefono;
    @FXML
    private Label labelProvincia;
    @FXML
    private Label labelFecha;
    @FXML
    private TableColumn<Persona, String> columnTelefono;
    @FXML
    private Label labelHijos;
    @FXML
    private HBox imagen;
    @FXML
    private ImageView imagenDB;
    @FXML
    private Button buttonCerrar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        columnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        columnApellidos.setCellValueFactory(new PropertyValueFactory<>("apellidos"));
        columnTelefono.setCellValueFactory(new PropertyValueFactory<>("Telefono"));
        columnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        columnProvincia.setCellValueFactory(
                cellData -> {
                    SimpleStringProperty property = new SimpleStringProperty();
                    if (cellData.getValue().getProvincia() != null) {
                        property.setValue(cellData.getValue().getProvincia().getNombre());
                    }
                    return property;
                });

        tableViewContactos.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    personaSeleccionada = newValue;
                    if (personaSeleccionada != null) {
                        textFieldNombre.setText(personaSeleccionada.getNombre());
                        textFieldApellidos.setText(personaSeleccionada.getApellidos());
                        textFieldTelefono.setText(personaSeleccionada.getTelefono());
                        textFieldEmail.setText(personaSeleccionada.getEmail());
                        labelProvincia.setText(personaSeleccionada.getProvincia().getNombre());
                        labelFecha.setText(personaSeleccionada.getFechaNacimiento().toString());
                        labelHijos.setText(personaSeleccionada.getNumHijos().toString());
                    } else {
                        textFieldNombre.setText("");
                        textFieldApellidos.setText("");
                        textFieldTelefono.setText("");
                        textFieldEmail.setText("");
                        labelHijos.setText("");
                        labelFecha.setText("");
                    }
                });
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public String convertStringToDate(Date indate) {
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
        /*you can also use DateFormat reference instead of SimpleDateFormat 
         * like this: DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");*/

        try {
            dateString = sdfr.format(indate);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return dateString;
    }

    public void cargarTodasPersonas() {
        Query queryPersonaFindAll = entityManager.createNamedQuery("Persona.findAll");
        List<Persona> listPersona = queryPersonaFindAll.getResultList();
        tableViewContactos.setItems(FXCollections.observableArrayList(listPersona));
    }

    /*    public void setTableViewPrevio(TableView tableViewPrevio) {
     this.tableViewPrevio = tableViewPrevio;
     }*/
    @FXML
    private void onActionButtonActualizar(ActionEvent event) {
        Alert alertActualizar = new Alert(AlertType.CONFIRMATION);
        alertActualizar.setTitle("Confirmar");
        alertActualizar.setHeaderText("¿Deseas Actualizar el siguiente registro?");
        alertActualizar.setContentText(personaSeleccionada.getNombre() + " "
                + personaSeleccionada.getApellidos());
        Optional<ButtonType> result = alertActualizar.showAndWait();
        if (result.get() == ButtonType.OK) {
            // Acciones a realizar si el usuario acepta
            entityManager.getTransaction().begin();
            entityManager.refresh(personaSeleccionada);
            entityManager.getTransaction().commit();
        } else {
            // Acciones a realizar si el usuario cancela
            int numFilaSeleccionada = tableViewContactos.getSelectionModel().getSelectedIndex();
            tableViewContactos.getItems().set(numFilaSeleccionada, personaSeleccionada);
            TablePosition pos = new TablePosition(tableViewContactos, numFilaSeleccionada, null);
            tableViewContactos.getFocusModel().focus(pos);
            tableViewContactos.requestFocus();
        }
        if (personaSeleccionada != null) {
            personaSeleccionada.setNombre(textFieldNombre.getText());
            personaSeleccionada.setApellidos(textFieldApellidos.getText());
            personaSeleccionada.setTelefono(textFieldTelefono.getText());
            personaSeleccionada.setEmail(textFieldEmail.getText());
            //   personaSeleccionada.setNumHijos(textFieldHijos.toShort());
            //   personaSeleccionada.setNumHijos(textFieldHijos.get);
            entityManager.getTransaction().begin();
            entityManager.merge(personaSeleccionada);
            entityManager.getTransaction().commit();

            int numFilaSeleccionada = tableViewContactos.getSelectionModel().getSelectedIndex();
            tableViewContactos.getItems().set(numFilaSeleccionada, personaSeleccionada);
            TablePosition pos = new TablePosition(tableViewContactos, numFilaSeleccionada, null);
            tableViewContactos.getFocusModel().focus(pos);
            tableViewContactos.requestFocus();
        }

    }

    @FXML
    private void onActionButtonNuevo(ActionEvent event) {
        try {
            // Cargar la vista de detalle
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PersonaDetalleView.fxml"));
            Parent rootDetalleView = fxmlLoader.load();
            PersonaDetalleViewController personaDetalleViewController = (PersonaDetalleViewController) fxmlLoader.getController();
            personaDetalleViewController.setRootContactosView(rootContactosView);
            personaDetalleViewController.setTableViewPrevio(tableViewContactos);

            // Código para el botón Nuevo
            personaSeleccionada = new Persona();
            personaDetalleViewController.setPersona(entityManager, personaSeleccionada, true);
                        //mostrar datos actuales.
            personaDetalleViewController.mostrarDatos();

            // Ocultar la vista de la lista
            rootContactosView.setVisible(false);

            // Añadir la vista de detalle al StackPane principal para que se muestre
            StackPane rootMain = (StackPane) rootContactosView.getScene().getRoot();
            rootMain.getChildren().add(rootDetalleView);
        } catch (IOException ex) {
            Logger.getLogger(ContactosViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonBorrar(ActionEvent event) {
        Alert alertBorrar = new Alert(AlertType.CONFIRMATION);
        alertBorrar.setTitle("Confirmar");
        alertBorrar.setHeaderText("¿Deseas Borrar el siguiente registro?");
        alertBorrar.setContentText(personaSeleccionada.getNombre() + " "
                + personaSeleccionada.getApellidos());
        Optional<ButtonType> result = alertBorrar.showAndWait();
        if (result.get() == ButtonType.OK) {
            // Acciones a realizar si el usuario acepta
            entityManager.getTransaction().begin();
            entityManager.remove(personaSeleccionada);
            entityManager.getTransaction().commit();

            tableViewContactos.getItems().remove(personaSeleccionada);

            tableViewContactos.getFocusModel().focus(null);
            tableViewContactos.requestFocus();
        } else {
            // Acciones a realizar si el usuario cancela
            int numFilaSeleccionada = tableViewContactos.getSelectionModel().getSelectedIndex();
            tableViewContactos.getItems().set(numFilaSeleccionada, personaSeleccionada);
            TablePosition pos = new TablePosition(tableViewContactos, numFilaSeleccionada, null);
            tableViewContactos.getFocusModel().focus(pos);
            tableViewContactos.requestFocus();
        }
    }

@FXML
    private void onActionButtonEditar(ActionEvent event) {
        if (personaSeleccionada != null) {
            try {
                // Cargar la vista de detalle
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PersonaDetalleView.fxml"));
                Parent rootDetallesView = fxmlLoader.load();
                PersonaDetalleViewController personaDetalleViewController = (PersonaDetalleViewController) fxmlLoader.getController();
                personaDetalleViewController.setRootContactosView(rootContactosView);
                personaDetalleViewController.setTableViewPrevio(tableViewContactos);

                // Código para el botón Editar
                personaDetalleViewController.setPersona(entityManager, personaSeleccionada, false);
                //mostrar datos actuales.
                personaDetalleViewController.mostrarDatos();

                // Ocultar la vista de la lista
                rootContactosView.setVisible(false);

                /*  if (personaSeleccionada != null) {
                rootContactosView.setVisible(false);
            } else {
                Alert alertBorrar = new Alert(AlertType.CONFIRMATION);
                alertBorrar.setTitle("Confirmar");
                alertBorrar.setHeaderText("¿Deseas Borrar el siguiente registro?");
                alertBorrar.setContentText(personaSeleccionada.getNombre() + " "
                        + personaSeleccionada.getApellidos());
            }*/
                // Añadir la vista de detalle al StackPane principal para que se muestre
                StackPane rootMain = (StackPane) rootContactosView.getScene().getRoot();
                rootMain.getChildren().add(rootDetallesView);
            } catch (IOException ex) {
                Logger.getLogger(ContactosViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setHeaderText("Porfavor selecciona primero un registro a editar");
            alert.showAndWait();

        }

    }

    @FXML
    private void onActionButtonCerrar(ActionEvent event) {
//        if (personaSeleccionada != null) {
//            rootContactosView.setVisible(false);
//            entityManager.getTransaction().commit();
//        } else {
//            rootContactosView.setVisible(true);
//            entityManager.getTransaction().rollback();
//        }

        Principal.primaryStage.close();

//        entityManager.close();
//        try {
//            DriverManager.getConnection("jdbc:derby:BDAgendaContactos;shutdown=true");
//        } catch (SQLException ex) {
//        }
//        System.exit(0);
    }
    

}
